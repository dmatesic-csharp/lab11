﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LAB11 {
    public partial class Form1 : Form {
        public Form1() {
            InitializeComponent();
            OsvjeziListu();
        }

        private void OsvjeziListu() {
            var notes = Studentss.Dohvati();
            lbStudents.Items.Clear();
            foreach (var note in notes) {
                lbStudents.Items.Add(note);
            }
        }

        private void lbStudents_MouseDoubleClick(object sender, MouseEventArgs e) {
            var user = (Students)lbStudents.SelectedItem;
            Form2 formEntrances = new Form2(user);
            formEntrances.ShowDialog();

        }
    }
}
