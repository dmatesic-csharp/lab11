using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;

namespace LAB11 {
    public partial class AttendDbContext : DbContext {
        public AttendDbContext()
            : base("name=AttendDbContext") {
        }

        public virtual DbSet<Attendances> Attendances { get; set; }
        public virtual DbSet<Students> Students { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder) {
            modelBuilder.Entity<Students>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<Students>()
                .Property(e => e.Surname)
                .IsUnicode(false);

            modelBuilder.Entity<Students>()
                .Property(e => e.Jmbag)
                .IsFixedLength();

            modelBuilder.Entity<Students>()
                .HasMany(e => e.Attendances)
                .WithRequired(e => e.Students)
                .HasForeignKey(e => e.IdStudent)
                .WillCascadeOnDelete(false);
        }
    }
}
