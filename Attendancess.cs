﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LAB11 {
    class Attendancess {

        public static IEnumerable<Attendances> GetAttendances(Students students) {
            var ctx = new AttendDbContext();

            return
              from e in ctx.Attendances
              where e.Students.Id == students.Id
              orderby e.PresentAt descending
              select e;
        }


        public static async void AddAttendances(Students students) {
            var ctx = new AttendDbContext();

            var attendance = new Attendances();
            attendance.IdStudent = students.Id;
            attendance.PresentAt = DateTime.Now;

            ctx.Attendances.Add(attendance);
            await ctx.SaveChangesAsync();
        }

    }
}
