﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LAB11 {
    public partial class Form2 : Form {
        public readonly Students students;
        public Form2() {
            InitializeComponent();
        }

        public Form2(Students students) : this() {
            this.students = students;
            OsvjeziListu();
        }

        public void OsvjeziListu() {
            lbPresent.Items.Clear();
            lbPresent.Items.AddRange(Attendancess.GetAttendances(students).ToArray());
        }

        private void btnAdd_Click(object sender, EventArgs e) {
            Attendancess.AddAttendances(students);
            lbPresent.Items.Clear();
            lbPresent.Items.AddRange(Attendancess.GetAttendances(students).ToArray());

        }
    }

}

