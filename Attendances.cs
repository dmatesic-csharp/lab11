namespace LAB11
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Attendances
    {
        public int Id { get; set; }

        public int IdStudent { get; set; }

        [Column(TypeName = "date")]
        public DateTime PresentAt { get; set; }

        public virtual Students Students { get; set; }

        public override string ToString() {
            return PresentAt.ToString() + Students.ToString();
        }

    }
}
